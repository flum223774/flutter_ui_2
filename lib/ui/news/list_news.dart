import 'dart:convert' as convert;

import 'package:basic_app/models/news_model.dart';
import 'package:basic_app/widgets/news_item.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import '../../configs/color.dart';

class ListNews extends StatefulWidget {
  @override
  _ListNewsState createState() => _ListNewsState();
}

class _ListNewsState extends State<ListNews> {
  List<NewsModel> news = [];

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this.fetchNews();
  }

  Future fetchNews() async {
    // .....
    int page = 0;
    var url = Uri.parse(
        'https://hn.algolia.com/api/v1/search?tags=story,author_pg&page=$page');
    try {
      http.Response res = await http.get(url);
      var jsonResponse = convert.jsonDecode(res.body);
      var newsData = jsonResponse['hits']
          .map<NewsModel>((e) => NewsModel.fromJson(e))
          .toList();
      setState(() {
        news = newsData;
      });
    } catch (e) {
      print(e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Hacker News'),
        backgroundColor: primaryColor,
      ),
      body: ListView.separated(
          itemCount: news.length,
          separatorBuilder: (BuildContext context, int index) => Divider(),
          itemBuilder: (BuildContext context, int index) =>
              NewsItem(model: news.elementAt(index))),
    );
  }
}
